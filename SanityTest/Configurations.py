def read_config_file(jsonfile):
    print("*****Reading config file*****)")

    f = open(jsonfile,'r')
    lines = f.read()

    print('Configs: ' + lines)

    self.config = {}

    self.cfg = json.loads(lines)
    f.close()

    keys_to_exist = [ 'user_login',
                      'password',
                      'prosody_log_location',
                      'server',
                      'database',
                      'ssml_folder_location',
                      'logs_folder_location
                    ]

    # Check if config file contains the keys and its values are non-empty
    for check_key in keys_to_exist:
        if not check_key in self.cfg.keys():
            print('[PREPARE_CONFIG] The key %s is missing from the config file'%check_key)
            exit(1)

    # Read the default values and check if empty
    for key in self.cfg.keys():
        if self.cfg[key] == '':
            print('[PREPARE_CONFIG] Empty value for key %r in config file'%key)
            exit(1)
        self.config[key] = self.cfg[key]

    logger.debug('Checking the existance of ProsodyMakerDaemon directory %r' % self.config['prosody_main_location'])
    assert(os.path.isdir(self.config['prosody_log_location']))   

    print('configuration file has %r' % self.config)

    return self.config

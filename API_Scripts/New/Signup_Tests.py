import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import random
import string
import configparser


class VBSignupSuites(unittest.TestCase):

    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_signup(self):
        #  Parameters
        random_email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        # password = "abc123"
        password = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        # username = "vghung"
        username = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        # last_name = "test123"
        last_name = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        # first_name = "test123"
        first_name = ''.join(random.choice(string.ascii_letters ) for ii in range(5))

        middle_name = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        nick_name = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        birth_date = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        default_language = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        gender = ''.join(random.choice(string.ascii_letters ) for ii in range(1))
        city = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        state = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        country = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        avatar_image_filename = ''.join(random.choice(string.ascii_letters ) for ii in range(5))
        profile_image_filename = ''.join(random.choice(string.ascii_letters ) for ii in range(5))

        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name,"middle_name": middle_name, "nick_name": nick_name, "birth_date": birth_date,
                "default_language": default_language, "gender": gender, "city": city, "state": state,
                "country": country, "avatar_image_filename": avatar_image_filename,
                "profile_image_filename": profile_image_filename}
        # middle_name; nick_name; birth_date; default_language;
        # gender; city; state; country; avatar_image_filename; profile_image_filename
        resp = Renderer_Functions.signup(user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["user"]["email_address"], email.lower()) is None) \
                        and (self.assertEqual(resp['results']["user"]["username"], username) is None) \
                        and (self.assertEqual(resp['results']["user"]["first_name"], first_name) is None) \
                        and (self.assertEqual(resp['results']["user"]["last_name"], last_name) is None) \
                        and (self.assertEqual(resp['results']["user"]["middle_name"], middle_name) is None) \
                        and (self.assertEqual(resp['results']["user"]["nick_name"], nick_name) is None) \
                        and (self.assertEqual(resp['results']["user"]["birth_date"], birth_date) is None) \
                        and (self.assertEqual(resp['results']["user"]["default_language"], default_language) is None) \
                        and (self.assertEqual(resp['results']["user"]["gender"], gender) is None) \
                        and (self.assertEqual(resp['results']["user"]["city"], city) is None) \
                        and (self.assertEqual(resp['results']["user"]["state"], state) is None) \
                        and (self.assertEqual(resp['results']["user"]["country"], country) is None) \
                        and (self.assertEqual(resp['results']["user"]["avatar_image_filename"], avatar_image_filename) is None) \
                        and (self.assertEqual(resp['results']["user"]["profile_image_filename"], profile_image_filename) is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status_code"]))
                    print("email: {0}".format(resp['results']["user"]["email_address"]))
                    print("username: {0}".format(resp['results']["user"]["username"]))
                    print("first_name: {0}".format(resp['results']["user"]["first_name"]))
                    print("last_name: {0}".format(resp['results']["user"]["last_name"]))
                    print("last_name: {0}".format(resp['results']["user"]["middle_name"]))
                    print("last_name: {0}".format(resp['results']["user"]["nick_name"]))
                    print("last_name: {0}".format(resp['results']["user"]["birth_date"]))
                    print("last_name: {0}".format(resp['results']["user"]["default_language"]))
                    print("last_name: {0}".format(resp['results']["user"]["gender"]))
                    print("last_name: {0}".format(resp['results']["user"]["city"]))
                    print("last_name: {0}".format(resp['results']["user"]["state"]))
                    print("last_name: {0}".format(resp['results']["user"]["country"]))
                    print("last_name: {0}".format(resp['results']["user"]["avatar_image_filename"]))
                    print("last_name: {0}".format(resp['results']["user"]["profile_image_filename"]))

            except urllibe.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_signup_invalid_email(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters  ) for ii in range(5))
        print("random_email: {0}".format(random_email))
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)
        print(resp)
        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["user"]["email_address"], email.lower()) is None) \
                        and (self.assertEqual(resp['results']["user"]["username"], username) is None) \
                        and (self.assertEqual(resp['results']["user"]["first_name"], first_name) is None) \
                        and (self.assertEqual(resp['results']["user"]["last_name"], last_name) is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status_code"]))
                    print("email: {0}".format(resp['results']["user"]["email_address"]))
                    print("username: {0}".format(resp['results']["user"]["username"]))
                    print("first_name: {0}".format(resp['results']["user"]["first_name"]))
                    print("last_name: {0}".format(resp['results']["user"]["last_name"]))

            except urllibe.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_3_signup_missing_email(self):
        # Parameters
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Missing Parameter email.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_4_signup_empty_email(self):
        # Parameters
        email = ""
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Incorrect value for email.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_5_signup_invalid_password(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters  ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        password = "abc"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["user"]["email_address"], email.lower()) is None) \
                        and (self.assertEqual(resp['results']["user"]["username"], username) is None) \
                        and (self.assertEqual(resp['results']["user"]["first_name"], first_name) is None) \
                        and (self.assertEqual(resp['results']["user"]["last_name"], last_name) is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status_code"]))
                    print("email: {0}".format(resp['results']["user"]["email_address"]))
                    print("username: {0}".format(resp['results']["user"]["username"]))
                    print("first_name: {0}".format(resp['results']["user"]["first_name"]))
                    print("last_name: {0}".format(resp['results']["user"]["last_name"]))

            except urllibe.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] != 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_signup_missing_password(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters  ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Missing Parameter password.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_7_signup_empty_password(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        password = ""
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Incorrect value for password.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_8_signup_empty_username(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters  ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        password = "abc123"
        username = ""
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Incorrect value for username.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_9_signup_missing_username(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        password = "abc123"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Missing Parameter username.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_10_signup_empty_lastname(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = ""
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Incorrect value for last_name.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_11_signup_missing_lastname(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        password = "abc123"
        username = "vghung"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Missing Parameter last_name.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_12_signup_empty_firstname(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = ""
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Incorrect value for first_name.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_13_signup_missing_firstname(self):
        # Parameters
        random_email = ''.join(random.choice(string.ascii_letters) for ii in range(5)) + '@speechmorphing.com'
        print("random_email: {0}".format(random_email))
        email = random_email
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status code"] != 200:
            try:
                if (self.assertEqual(resp["status code"], 400)) is None \
                        and (self.assertEqual(resp["error message"],
                                              "Missing Parameter first_name.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp["status code"]))
                    print("Reason: {0}".format(resp["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] == 200:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_20_already_registered(self):
        #  Parameters
        email = "hung@speechmorphing.com"
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name, "middle_name": "koko", "country": "us"}
        # middle_name; nick_name; birth_date; default_language;
        # gender; city; state; country; avatar_image_filename; profile_image_filename
        resp = Renderer_Functions.signup(user)
        print(resp)
        # here we will check if the request is passed or failed
        if resp["status code"] == 400:
            try:
                if (self.assertEqual(resp['error message']["status_code"], "UserAlreadyRegistered")) is None \
                        and (self.assertEqual(resp["error message"]['details'],
                                              'User already registered') is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status code: {0}".format(resp['error message']["status_code"]))
                    print("Reason: {0}".format(resp['error message']["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status code"] != 400:
            print("Negative Test Failed")

        # Check error type
        elif resp["status code"] != 200:
            print("-|-Result:Test case Failed")
            print("status code: {0}".format(resp["status code"]))
            print("Reason: {0}".format(resp["details"]))

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)

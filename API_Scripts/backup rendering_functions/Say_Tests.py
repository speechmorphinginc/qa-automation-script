import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from Database import Postgres
import JSON_Request as Data_Utils
import WAV_Logs as wav
import random

class VBSaySuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_say(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        # request_id = "654334"
        # #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # #file_format = "wav"

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" ' + \
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that ' + \
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing a new input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute("select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        print("***********VALIDATING API RESPONSE**********")
        if resp == None:
            print("-----No response back-----")

        print("Response Status Code: {0}".format(resp["status_code"]))
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn(".wav", resp["results"]["filename"]) is None :
            # if wav.check_wav_files(resp,"",True):
                print ("Get Voice Pass")
                print ("resp: {0}".format(resp))
                print ("status_code: {0}".format(resp["status_code"]))
                print ("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

    def test_positive_15_pcmu(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a water fountain?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        file_format = "pcmu"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "file_format": file_format}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        print("***********VALIDATING API RESPONSE**********")
        if resp == None:
            print("-----No response back-----")

        print("Response Status Code: {0}".format(resp["status_code"]))
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None:
            if wav.check_wav_files(resp,"",True):
                print ("Get Voice Pass")
                print ("resp: {0}".format(resp))
                print ("status_code: {0}".format(resp["status_code"]))
                print ("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

    def test_negative_2_say_wrong_smi_voice_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "ABCD"
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for smi_voice_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_say_missing_smi_voice_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter smi_voice_id") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_4_say_wrong_request_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037928290"
        request_id = 123456
        smi_domain_id = "72057594037927948"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for request_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_5_say_missing_request_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037927940"
        smi_domain_id = "72057594037927948"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter request_id") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_say_wrong_domain_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "A"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_7_say_missing_domain_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter smi_domain_id") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_8_say_wrong_text_sml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037928290"
        request_id = "215020"
        smi_domain_id = "72057594037927948"
        text_ssml = 123456789
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for text_ssml.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_9_say_missing_text_ssml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "1"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter text_ssml.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_10_say_wrong_login(self):
        # Parameters
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))


            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_11_say_missing_login(self):
        # Parameters
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_12_say_no_permission_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037928290"
        request_id = "215020"
        smi_domain_id = "72057594037927940"
        text_ssml = ""
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              " you do not have permission to use this domain. ") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_13_say_no_permission_voice(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037927953"
        request_id = "215020"
        smi_domain_id = "72057594037927948"
        text_ssml = "special ssml"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              " you do not have permission to use this voice. ") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_14_say_no_permission_style_gest_mood(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak version="1.0" xml:lang="0" xml:domain="72057594037927951" xml:accent="default" xml:rate="100" xml:volume="100" xml:pitch="100" xml:style="72057594037927941" xml:mood="undefined" xml:voice="72057594037928290"><gesture value="6"></gesture>wild and free</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "abc"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        print(user)
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for smi_voice_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_15_say_wrong_file_format(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        smi_voice_id = "72057594037928290"
        request_id = "654334"
        smi_domain_id = "72057594037927948"
        file_format = "1234"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "file_format": file_format}
        resp = Renderer_Functions.say(resp["cookies"][0], user)

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              " the accepted values for format are wav, pcma, pcmu.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        smi_voice_id = "72057594037927940"
        request_id = "215020"
        smi_domain_id = "1"
        text_ssml = "<? xml version=\"1.0\" encoding=\"UTF-8\"?><speak version=\"1.0\"xml:lang=\"en-US\">That is a big </speak>"
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml}
        resp = Renderer_Functions.say("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
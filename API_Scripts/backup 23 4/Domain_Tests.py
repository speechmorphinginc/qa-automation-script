import urllib.error as urllibe
import unittest
import Renderer_Functions
import configparser
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBDomainSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print( self.currentResult)

    def test_positive_1_domain(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # # Get Random Language id
        # cursor = Postgres.connect_to_db()
        # # select only the language_id's that have domains
        # cursor.execute('SELECT DISTINCT language_id FROM smi_domain')
        # db_info = [r for r in cursor]
        # random_line = randint(0, cursor.rowcount-1)
        #
        # language_id = db_info[random_line][0]
        language_id = 6

        # Send to Domain
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"language_id": language_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # Fetch info from DB
        # allowed_domains = Renderer_Functions.get_allowed_domains_in_allowed_projects_in_allowed_accounts(email, language_id)
        allowed_domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)

        # Fetch full domain info from DB to be compare with the API results
        cursor = Postgres.connect_to_db()
        str = "("
        for i in allowed_domains:
            str += "'" + repr(i[0]) + "', "
        str = str[:-2]
        str += ")"
        query = "select * from smi_domain where smi_domain_id in " + str
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])
        db_info = [x for x in db_info if x[4] == language_id]

        # Reorder the API received rows

        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # here we will check if the request is passed or failed
        count = resp.__len__()
        for x in range(count):
            print(x)
            print (resp[x]["smi_domain_id"], repr(db_info[x][0]))
            print(resp[x]["name"], repr(db_info[x][1]))
            print(resp[x]["mood_id"], repr(db_info[x][11]))
            print(resp[x]["pitch"], repr(db_info[x][6]))
            print(resp[x]["volume"], repr(db_info[x][7]))
            print(resp[x]["style_id"], repr(db_info[x][3]))
            print(resp[x]["speed"], repr(db_info[x][5]))
            print(resp[x]["normalization_domain_id"], repr(db_info[x][8]))
            print(resp[x]["language_id"], repr(db_info[x][4]))
            print(resp[x]["smi_domain_owner_project_id"], repr(db_info[x][10]))
            print(resp[x]["open_to_public"], repr(db_info[x][9]))
            print(resp[x]["supported"], repr(db_info[x][12]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                     repr(db_info[x][0])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["name"],
                                                                     repr(db_info[x][1])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["mood_id"],
                                                                     repr(db_info[x][11])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["open_to_public"],
                                                                     repr(db_info[x][10])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["pitch"],
                                                                     repr(db_info[x][6])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["supported"],
                                                                     repr(db_info[x][11])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["volume"],
                                                                     repr(db_info[x][7])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["style_id"],
                                                                     repr(db_info[x][3])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["speed"],
                                                                     repr(db_info[x][5])))
                    and (
                    Renderer_Functions.compare_two_values_equal(resp[x]["normalization_domain_id"],
                                                                repr(db_info[x][8])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["language_id"],
                                                                     repr(db_info[x][4])))):
                print ("Domain Pass")
                print ("resp: {0}".format(resp))

    def test_positive_2_open_to_public(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        send_unsupported = False
        open_to_public = False

        # Send to Domain
        # allowed_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"language_id": language_id , "project_id": random_project, "send_unsupported": send_unsupported, "open_to_public": open_to_public}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # Fetch the info from the Domain table
        cursor = Postgres.connect_to_db()
        query = "SELECT smi_domain_id, domain_name, parent_id, style_id, speed, pitch, volume, normalization_domain_id, open_to_public, mood_id, supported FROM smi_domain WHERE language_id=" + str(language_id) + " AND supported=" + str(send_unsupported) + " AND open_to_public=" + str(open_to_public)
        cursor.execute(query)

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # -------------------
        # # Get logged in email
        # config = configparser.ConfigParser()
        # config.sections()
        # config.read('api_config.ini')
        # email = config["LOGIN"]["email"]
        #
        # cursor.execute(
        #     "WITH webSupported as (SELECT DISTINCT smi_domain_id FROM canned_clip_website WHERE supported)SELECT smi_domain_id,domain_name as name, language_id, parent_id as parent_smi_domain_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, supported, smi_domain_owner_email, string_agg(keyword, ', ') as keywords, string_agg(user_email, ', ') as email, websupported.smi_domain_id is not NULL web_supported   FROM smi_domain LEFT OUTER JOIN smi_domain_permissions USING (smi_domain_id) LEFT OUTER JOIN smi_domain_keywords USING (smi_domain_id) LEFT OUTER JOIN websupported using (smi_domain_id) WHERE (open_to_public OR smi_domain_owner_email = '" + email + "'  OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0  OR trim(domain_name) ~* '()' OR smi_domain_id IN (SELECT smi_domain_id FROM smi_domain_keywords  WHERE  keyword ~* '^()$')) THEN TRUE ELSE FALSE END GROUP BY smi_domain_id, domain_name, language_id, parent_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, smi_domain_owner_email, websupported.smi_domain_id   ORDER BY (websupported.smi_domain_id is NOT NULL) DESC, domain_name")
        # db_info = [r for r in cursor]
        # db_info = sorted(db_info, key=lambda k: k[0])
        # -----------------------

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # Here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if (resp.__len__() != db_info.__len__()):
            assert False
        count = resp.__len__()
        for x in range(count):
            print(x)
            print(resp[x]["smi_domain_id"], repr(db_info[x][0]))
            print(resp[x]["name"], repr(db_info[x][1]))
            print(resp[x]["mood_id"], repr(db_info[x][9]))
            print(resp[x]["pitch"], repr(db_info[x][5]))
            print(resp[x]["volume"], repr(db_info[x][6]))
            print(resp[x]["style_id"], repr(db_info[x][3]))
            print(resp[x]["speed"], repr(db_info[x][4]))
            print(resp[x]["normalization_domain_id"], repr(db_info[x][7]))
            print(resp[x]["open_to_public"], repr(db_info[x][8]))
            print(resp[x]["supported"], repr(db_info[x][10]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                     repr(db_info[x][0])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["name"],
                                                                     repr(db_info[x][1])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["mood_id"],
                                                                     repr(db_info[x][9])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["open_to_public"],
                                                                     repr(db_info[x][8])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["pitch"],
                                                                     repr(db_info[x][5])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["supported"],
                                                                     repr(db_info[x][10])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["volume"],
                                                                     repr(db_info[x][6])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["style_id"],
                                                                     repr(db_info[x][3])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["speed"],
                                                                     repr(db_info[x][4])))
                    and (
                    Renderer_Functions.compare_two_values_equal(resp[x]["normalization_domain_id"],
                                                                repr(db_info[x][7])))):
                print ("Domain Pass")
                print ("resp: {0}".format(resp))

    def test_negative_3_domain_incorrect_language_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 99999
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, 6)
        random_project = random.choice(allowed_projects)
        user = {"language_id": language_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")
        print(resp["status_code"])
        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["count"], 0) is None):
                        # and (self.assertEqual(resp['results']["error message"],
                        #                       "Incorrect value for language_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["count"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_4_domain_missing_project_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        user = {"language_id": language_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter project_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_5_domain_missing_language_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter language_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_6_domain_wrong_login(self):
        # Parameters
        user = {"language_id": 2, "send_unsupported": True}
        resp = Renderer_Functions.send_request("123456789", user, "domain")
        print(resp)
        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_7_domain_missing_login(self):
        # Parameters
        user = {"language_id": 2, "send_unsupported": True}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_8_expired_token(self):
        # Parameters
        user = {"language_id": 6, "send_unsupported": True}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")
            

if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
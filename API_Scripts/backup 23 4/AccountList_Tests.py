import urllib.error as urllibe
import unittest
import Renderer_Functions
from Database import Postgres

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBAccountlistSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult))
        print (self.currentResult)


    def test_positive_1_accountlist(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "accountlist")

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()
        cursor.execute( 'SELECT distinct A.account_name, A.account_id, A.active, B.user_permission_id FROM account A, ' 
                        'user_account_permissions B where A.account_id = B.account_id and A.account_owner_user_email = '
                        'B.user_email order by account_name')

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[1])

        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["account_id"])

        # here we will check if the request is passed or failed
        count = resp.__len__()
        for x in range(count):
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["account_id"], db_info[x][1]) is None) \
                    and (self.assertEqual(resp[x]["account_name"], db_info[x][0]) is None) :
                print("resp: {0}".format(resp))
                print("account_id: {0}".format(resp[x]["account_id"]))
                print("account_name: {0}".format(resp[x]["account_name"]))
        print("Account List Pass")

    def test_negative_2_accountlist_wrong_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("123456789", user, "accountlist")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_3_accountlist_missing_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "accountlist")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_4_accountlist_expired_token(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "accountlist")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)

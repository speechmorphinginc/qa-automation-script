import unittest
import Renderer_Functions as Renderer_Functions
from random import randint
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBTextlistsSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}


    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult))
        print(self.currentResult)


    def test_positive_1_textlists(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains2(email, language_id)
        #allowed_projects = Renderer_Functions.get_allowed_projects_in_DB(email)
        random_project = random.choice(allowed_projects)
        random_project='C4398046511113-4398046511123'

        user = {"project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "textlists")

        # Fetch the info from Database
        cursor = Postgres.connect_to_db()
        # query = "SELECT distinct renderer_text_id, name, text_ssml, normalization_domain_id, open_to_public FROM renderer_text where text_owner_email = '" + email + "'"
        query = "SELECT distinct renderer_text_id, name, text_ssml, open_to_public FROM renderer_text where text_owner_project_id = '" + random_project+ "' ORDER BY name"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k['text_id'])

        # Choose a random row to test
        random_line = randint(0, len(db_info) - 1)

        # here we will check if the request is passed or failed
        count = resp.__len__()
        for x in range(count):
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[random_line]["text_id"], str(db_info[random_line][0])) is None) \
                    and (self.assertEqual(resp[random_line]["text_ssml"], db_info[random_line][2]) is None) \
                    and (self.assertEqual(resp[random_line]["name"], db_info[random_line][1]) is None):
                 print("resp: {0}".format(resp))
                # print("Text_Id: {0}".format(resp[random_line]["text_id"]))
                # print("Text_Ssml: {0}".format(resp[random_line]["text_ssml"]))
                # print("Name: {0}".format(resp[random_line]["name"]))
            else:
                print("Positive Test Failed")
                assert False

    print("Textlists Pass")

    def test_negative_2_textlists_wrong_login(self):
        # Parameters
        user = {}
        resp = Renderer_Functions.send_request("123456789", user, "textlists")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], '400')) is None \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_textlists_missing_login(self):
        # Parameters
        user = {}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "textlists")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], '400')) is None \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)

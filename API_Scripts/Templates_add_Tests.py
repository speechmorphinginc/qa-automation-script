import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBtemplatesaddSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_templates_add(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."
        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "API_testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))+"."

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="6"  xml:domain="' + str(domain_id) + '"' \
                     ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" xml:sub_fields_only="yes" >' + plain_text_ssml + '</speak> '
       # text_ssml= '<speak>Youre afraid to talk to a guy you idolize.</speak>'

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id, "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()
        query = "select * from renderer_text where name ilike '" + template_name + "'"
        cursor.execute(query)
        db_info = [r for r in cursor]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["template_id"], str(db_info[0][6])) is None) \
            and (self.assertEqual(db_info[0][1], template_name) is None) \
            and (self.assertEqual(db_info[0][2], text_ssml) is None) \
            and (self.assertEqual(db_info[0][4], project_id) is None) \
            and (self.assertEqual(db_info[0][8], domain_id) is None) \
            and (self.assertEqual(db_info[0][11], email) is None):
            print("resp: {0}".format(resp))

            f = open("Added_Parameters.txt", "a+")
            f.write("\n")
            f.write("template_id: {0} " + resp["results"]["template_id"])
            f.write("\n")
            f.write("template_name: {0} " + template_name)
            f.write("\n")
            f.close()
        else:
            print("Positive Test Failed")
            assert False
        print("Templates Add Pass")

    def test_negative_2_templates_add_wrong_login(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(
            random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="'+str(domain_id)+'"' \
                    ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >'+plain_text_ssml+'</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                  "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample",
                  "sensitivity_next_word": False,
                  "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
            ]
                  },
                 {
                     "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                     "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text",
                     "sensitivity_next_word": False,
                     "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                     {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                 ]
                 }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request("123456789", user, "templatesadd")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_templates_add_missing_login(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(
            random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(
            domain_id) + '"' \
                         ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                  "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample",
                  "sensitivity_next_word": False,
                  "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
            ]
                  },
                 {
                     "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                     "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text",
                     "sensitivity_next_word": False,
                     "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                     {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                 ]
                 }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "templatesadd")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_templates_add_expired_token(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(
            random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                    ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                  "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample",
                  "sensitivity_next_word": False,
                  "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
            ]
                  },
                 {
                     "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                     "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text",
                     "sensitivity_next_word": False,
                     "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                     {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                     {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                 ]
                 }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "templatesadd")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_templates_missing_text_ssml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter text_ssml.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_6_templates_wrong_text_ssml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = 123456789

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for text_ssml.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_7_templates_missing_domain_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(1) + '"' \
                         ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter domain_id.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_8_templates_wrong_domain_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        domain_id = "wrong"

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                    ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], " incorrect value for domain_id.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_9_templates_wrong_template_name(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = 123456

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                    ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for template_name.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_10_templates_missing_template_name(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                    ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter template_name.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_11_templates_wrong_is_public(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        is_public = "wrong"

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                    ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for is_public.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_12_templates_missing_is_public(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                    ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter is_public.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_13_templates_wrong_language_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                    ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        language_id = "wrong"
        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for language_id.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_14_templates_missing_language_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                      ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter language_id.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_15_templates_wrong_project_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        project_id = "wrong"

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                      ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        language_id = 6

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect project_id ") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_16_templates_missing_project_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                         ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        language_id = 6

        user = {"text_ssml": text_ssml, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter project_id.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_17_templates_wrong_words(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        words = "wrong"

        language_id = 6

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                         ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for words. words array cannot be empty  phonemes array cannot be empty for all words. ") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_18_templates_missing_words(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        words = "wrong"

        language_id = 6

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                         ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter words. words array cannot be empty  phonemes array cannot be empty for all words. ") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_19_templates_incorrect_text_ssml_format(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id,
                "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesadd")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"],' incorrect format for the text_ssml.') is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)

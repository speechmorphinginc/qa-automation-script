import unittest
import Renderer_Functions as Renderer_Functions

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBShareSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)

    def test_positive_1_share(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        edits_allowed = "edit"
        notes = "For internal only"
        text_id = 72057594037927992
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(resp["results"]["status"], "success")) is None:
            print ("Get Voice Pass")
            print ("resp: {0}".format(resp))
            print ("status_code: {0}".format(resp["status_code"]))
            print ("status: {0}".format(resp["results"]["status"]))
        else:
            print("Positive Test Failed")
            assert False
        print("Share Pass")

    def test_negative_2_share_incorrect_email_length(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = "j"
        edits_allowed = "edit"
        notes = "For internal only"
        text_id = 72057594037927992
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "incorrect value for emails_length.The length of emails array  and email_length parameter does not have the same value.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_missing_Parameter_email_length(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        edits_allowed = "edit"
        notes = "For internal only"
        text_id = 72057594037927992
        user = {"emails": emails,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "missing parameter emails_length.The length of emails array  and email_length parameter does not have the same value.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_share_incorrect_edits_allowed(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        edits_allowed = 123
        notes = "For internal only"
        text_id = 72057594037927992
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "incorrect value for edits_allowed.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_5_share_incorrect_email_length(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        notes = "For internal only"
        text_id = 72057594037927992
        user = {"emails": emails, "emails_length": emails_length,
                "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "missing parameter edits_allowed.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False


    def test_negative_6_share_incorrect_notes(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        edits_allowed = "edit"
        notes = 111
        text_id = 72057594037927992
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "incorrect value for notes.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_7_share_missing_notes(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        edits_allowed = "edit"
        text_id = 72057594037927992
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "missing parameter notes.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_8_share_incorrect_text_id(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        edits_allowed = "edit"
        notes = "For internal only"
        text_id = "abc"
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "incorrect value for text_id.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_9_share_missing_text_id(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        edits_allowed = "edit"
        notes = "For internal only"
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "missing parameter text_id.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_10_share_missing_emails(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        emails_length = 2
        edits_allowed = "edit"
        notes = "For internal only"
        text_id = 4398046513826
        user = {"emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"],
                                      "missing parameter emails.") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_11_share_wrong_login(self):
        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        edits_allowed = "edit"
        notes = "For internal only"
        text_id = 72057594037927992
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request("123456789", user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], '401')) is None \
                and (self.assertEqual(resp["details"],
                                      "Unauthorized Access") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_12_share_missing_login(self):
        # Parameters
        emails = ["hung@speechmorphing.com", "gordon@speechmorphing.com"]
        emails_length = 2
        edits_allowed = "edit"
        notes = "For internal only"
        text_id = 72057594037927992
        user = {"emails": emails, "emails_length": emails_length,
                "edits_allowed": edits_allowed, "notes": notes, "text_id": text_id}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "share")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], '401')) is None \
                and (self.assertEqual(resp["details"],
                                      "Unauthorized Access") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)

import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint


class VBGetvoicesSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    # without any optional parameters, we should get all the voices
    def test_positive_1_getvoices_all(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    user = {"send_unsupported": True}
                    resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

                    # Fetch the info from the Age table
                    cursor = Renderer_Functions.connect_to_db()
                    cursor.execute(
                        "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = 'hung@speechmorphing.com' OR user_email ILIKE 'hung@speechmorphing.com') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])

                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["Results"], key=lambda k: k['smi_voice_id'])

                    # Choose a random row to test
                    random_line = randint(0, len(db_info) - 1)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp[random_line]["smi_voice_id"], str(db_info[random_line][0])) is None) \
                            and (self.assertEqual(resp[random_line]["name"], db_info[random_line][1]) is None):
                        print "Get Voice Pass"
                        print "resp: {0}".format(resp)
                        print "status_code: {0}".format(resp_status_code)

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    # options: gender_id
    def test_positive_2_getvoices_gender_id(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    user = {"gender_id": 2, "send_unsupported": True}
                    resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp["Results"][0]["smi_voice_id"], "72057594037927944") is None) \
                            and (self.assertEqual(resp["Results"][0]["name"], "Alon") is None):
                        print "Get Voice Pass"
                        print "resp: {0}".format(resp)
                        print "status_code: {0}".format(resp["status_code"])

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    # options: age_id
    def test_positive_3_getvoices_age_id(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    user = {"age_id": 5, "send_unsupported": True}
                    resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp["Results"][0]["smi_voice_id"], "72057594037927939") is None) \
                            and (self.assertEqual(resp["Results"][0]["name"], "Gabi-2000") is None) \
                            and (self.assertEqual(resp["Results"][1]["smi_voice_id"], "72057594037927945") is None) \
                            and (self.assertEqual(resp["Results"][1]["name"], "Jelena") is None):
                        print "Get Voice Pass"
                        print "resp: {0}".format(resp)
                        print "status_code: {0}".format(resp["status_code"])

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_positive_4_getvoices_language_id(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    user = {"language_id": 2, "send_unsupported": True}
                    resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp["Results"][0]["smi_voice_id"], "72057594037927939") is None) \
                            and (self.assertEqual(resp["Results"][0]["name"], "Gabi-2000") is None) \
                            and (self.assertEqual(resp["Results"][1]["smi_voice_id"], "72057594037927948") is None) \
                            and (self.assertEqual(resp["Results"][1]["name"], "Hazel") is None):
                        print "Get Voice Pass"
                        print "resp: {0}".format(resp)
                        print "status_code: {0}".format(resp["status_code"])

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_5_getvoices_missing_sendUnsupported(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    user = {}
                    resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Missing Parameter sendUnsupported.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_6_getvoices_wrong_login(self):
                    # parameters
                    user = {"send_unsupported": True}
                    resp = Renderer_Functions.get_voices("123456789", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

    def test_negative_7_getvoices_missing_login(self):
                    # parameters
                    user = {"send_unsupported": True}
                    resp = Renderer_Functions.get_voices("NO TOKEN", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)

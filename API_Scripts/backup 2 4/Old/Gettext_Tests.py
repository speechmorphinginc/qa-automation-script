import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint

class VBGettextSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_getext(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    # filename id
                    # filename_id = 72057594037927937
                    # fetch filename id from filename functions
                    template = "NO"
                    user = {"normalization_domain_id": 1, "template": template}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["Results"][1]["list"]
                    filename_id = resp_extract[0]["Filename_Id"]

                    # parameters
                    user = {"filename_id": filename_id}
                    resp = Renderer_Functions.gettext(resp["cookies"][0], user)

                    # Fetch the info from Database
                    cursor = Renderer_Functions.connect_to_db()
                    cursor.execute("SELECT  renderer_text_id, name, text_ssml,open_to_public,template_id, is_template_org  FROM renderer_text where renderer_text_id = " + filename_id + "")

                    db_info = [r for r in cursor]
                    # db_info = sorted(db_info, key=lambda k: k[0])

                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    # resp = sorted(resp["Results"], key=lambda k: k['Text_Id'])
                    resp = resp["Results"]

                    # Choose a random row to test
                    random_line = randint(0, len(db_info) - 1)

                    # here we will check if the voice detail is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp["Text_Id"], str(db_info[random_line][0]))) is None \
                            and (self.assertEqual(resp["Text_Ssml"],
                                db_info[random_line][2])) is None \
                            and (self.assertEqual(resp["Name"], db_info[random_line][1])) is None \
                            and (self.assertEqual(resp["Permission"], str(db_info[random_line][3]).lower())) is None:
                        print "Get Voice Pass"
                        print "resp: {0}".format(resp)
                        print "status_code: {0}".format(resp_status_code)
                        print "Text_Id: {0}".format(resp["Text_Id"])
                        print "Text_Ssml: {0}".format(resp["Text_Ssml"])
                        print "Name: {0}".format(resp["Name"])
                        print "Permission: {0}".format(resp["Permission"])

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_2_gettext_wrong_login(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    # filename id
                    # filename_id = 72057594037927937
                    # fetch filename id from filename functions
                    template = "NO"
                    user = {"normalization_domain_id": 1, "template": template}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["Results"][1]["list"]
                    filename_id = resp_extract[0]["Filename_Id"]

                    # parameters
                    user = {"filename_id": filename_id}
                    resp = Renderer_Functions.gettext("123456789", user)
                    # resp_results = resp["Results"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_3_gettext_missing_login(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # parameters
                    # filename id
                    # filename_id = 72057594037927937
                    # fetch filename id from filename functions
                    template = "NO"
                    user = {"normalization_domain_id": 1, "template": template}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["Results"][1]["list"]
                    filename_id = resp_extract[0]["Filename_Id"]

                    # parameters
                    user = {"filename_id": filename_id}
                    resp = Renderer_Functions.gettext("NO TOKEN", user)
                    # resp_results = resp["Results"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)

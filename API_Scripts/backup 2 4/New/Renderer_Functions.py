import urllib.request as urllibr
import urllib.error
import urllib.parse as urllibp
import json
import requests
import os
import sys
# import grequests
import random
from random import randrange
from socket import timeout
import re
import configparser
import Logger as Loggit
import SMI_API as SMI_API

# baseUrl = "http://50.197.139.145:8280"
# baseUrl = "http://ec2-52-53-250-125.us-west-1.compute.amazonaws.com:8080/"
baseUrl = "http://10.1.10.131:8080"
#baseUrl = "http://smorph-demo-1608781781.us-east-1.elb.amazonaws.com"
dbUrl = "10.1.10.131"
#dbUrl = "smorph-demo-gpu.clx3qujijubm.us-east-1.rds.amazonaws.com"
# baseUrl = "ec2-54-67-28-43.us-west-1.compute.amazonaws.com"
# baseUrl = "http://smorph-demo-1608781781.us-east-1.elb.amazonaws.com/RenderingApp/"
# baseUrl = "https://smorph-say.speechmorphing.com:8443"
# baseUrl = "http://smorph-dev.speechmorphing.com:9090" #feedback
boundary = ""

config = configparser.ConfigParser()
config.sections()
config.read('api_config.ini')
logger = Loggit.init_logger('API_TEST',config['LOCATION']['logs'])

def login(user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    print(config["SERVER"]["url"])
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "login")
    request = urllibr.Request(url)

    # add header to request
    headers = {"Content-Type": "application/json"}
    for key, value in headers.items():
        request.add_header(key, value)

    # t = api_request(user_json, request, url, headers)

    # post json data
    response = requests.post(url, user_json, headers)
    if response is None:
        print("*****No response from API*****")
    else:
        print(response.content.decode("utf-8","strict"))
        content_json = json.loads(response.content.decode("utf-8","strict"))
        print ("response.status_code {0}".format(response.status_code))

    print(response.status_code)

    if response.status_code == 200:
        try:
            resp = {}
            resp["status_code"] = response.status_code
            resp["content"] = content_json
            print(content_json)
            resp["cookies"] = response.cookies.values()
            return resp
        except:
            resp = {}
            resp["status_code"] = response.status_code
            resp["error_code"] = content_json["results"]["error code"]
            resp["details"] = content_json["results"]["error message"]
            return resp
    else:
        resp = {}
        resp["status_code"] = response.status_code
        print(content_json["results"])
        resp["details"] = content_json["results"]["error message"]
        return resp

        '''
        if response.status_code == 200:
            resp = {}
            resp["status_code"] = response.status_code
            resp["content"] = content_json
            resp["cookies"] = response.cookies.values()
            return resp

        else:
            resp = {}
            resp["status_code"] = response.status_code
            resp["error_code"] = content_json["Results"]["Error Code"]
            resp["details"] = content_json["Results"]["Error Message"]
            return resp
        '''

def signup(user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "signup")
    request = urllibr.Request(url)
    
    # add header to requests
    headers = {"Content-Type": "application/json"}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def voice_details(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "voicedetails")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def get_voices(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "voices")
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def domain(token, user):

    user_json = json.dumps(user).encode("utf-8")
    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "domain")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    t = api_request(user_json, request, url, headers)
    return t


def filename(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "filenames")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def gettext(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "text")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def savetext(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = config["SERVER"]["url"] + "/renderer/savetext"
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def share(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = config["SERVER"]["url"] + "/renderer/share"
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def feedback(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = config["SERVER"]["url"] + "/vb/feedback"
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def say(token, user):
    user_json = json.dumps(user).encode("utf-8")
    #print(user_json)
    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "say")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def say_templates(token, user):
    user_json = json.dumps(user).encode("utf-8")
    #print(user_json)
    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "templates")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def user_group(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = config["SERVER"]["url"] + "/vb/usergroup"
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def gesture(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "gesture")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def keyword(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "keyword")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def lingo(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "lingo")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def mood(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "mood")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def style(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "style")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def gender(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "gender")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def language(token, user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "language")
    print(url)
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)

    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def age(token, user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "age")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)

    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def textlists(token, user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = config["SERVER"]["url"] + "/renderer/textlists"
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def word_transcription(token, user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "wordtranscription")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def word_replacement(token, user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "wordreplacement")

    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def logout(token, user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "logout")
    #url = config["SERVER"]["url"] + "/smorphing/1.0/logout"
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def svp(token):

    user_json = {}
    
    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "svp")
    #url = config["SERVER"]["url"] + "/smorphing/1.0/logout"
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def version(token, user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = config["SERVER"]["url"] + "/vb/getMyDetails"
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    response = requests.get(url, headers)
    resp = {}
    resp["status_code"] = response.status_code
    resp["details"] = json.loads(response.content)
    print (resp["details"])
    return resp

def jump(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'jump')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def stop(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'stop')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def stop(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'stop')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def changerate(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'changerate')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def cancel(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'cancel')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def lexicon(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'lexicon')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def word_replacement(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'wordreplacement')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def word_replacement_add(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'wordreplacementadd')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def word_replacement_del(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'], 'wordreplacementdel')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def word_transcription(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'wordtranscription')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def word_transcription_add(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'wordtranscriptionadd')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

def word_transcription_del(token, user):

    user_json = json.dumps(user).encode("utf-8")
    
    # create request
    #url = baseUrl + "/renderer/voices"
    url = SMI_API.endpoint_switch(config['SERVER']['url'],'wordtranscriptiondel')
    request = urllibr.Request(url)
        
    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

         
def api_request(user_json, request, url, headers):
    try:
        print("Getting response...")
        response = urllibr.urlopen(request, user_json, timeout=30)
        print("*******************")
        geronimo = response.read().decode("utf-8",'strict')
        print(geronimo)
        print("*******************")
        try:
            resp = json.loads(geronimo)
        except:
            logger.error("Parsing Error: JSON failed to load")
        #resp = null
        print("*******************")
        # this below "try" code is only for signup tests
        try:
            if resp["status code"] == 400:
                return resp
            
        except:
                
            resp["status_code"] = response.code
            #print('HTTPError: {}'.format(response.code))
            return resp

        
    except requests.exceptions.ConnectionError as disconnect:
        logger.error ('Server Down', disconnect)
        resp = None
        return resp

    except (urllib.error.HTTPError, urllib.error.URLError) as error:
        logger.error ('Data not retrieved because of: %s', error)
        resp = {}
        code = str(error).split(" ")
        regex = re.compile('[^0-9]')
        print(code)
        sc = regex.sub('', code[len(code)- 2])
        resp["status_code"] = sc
        resp["details"] = 'Unauthorized Access'
        return resp

    except timeout:
        logger.error ('socket timed out - URL')
        resp = None
        return resp

    except:  # if the answer above is different than 200 then use the second method in order to get the reason
        response = requests.post(url, user_json, headers)
        print(response)
        resp = {}
        resp["status_code"] = response.status_code
        print("Response Code: " + str(resp["status_code"]))
        if response.status_code != 404:
            content_json = json.loads(response.content)
            
            try:
                resp["details"] = content_json["results"]["error_message"]
                logger.error (resp["details"])
            except:  # the tests that uses sendUnsupported have the error in differnt path
                resp["details"] = content_json["Error"]
                logger.error(resp["details"])
        return resp


import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import string

class VBFeedbackSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        print( self.currentResult)

    def test_positive_1_feedback(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user_id = "112233445566"
                    reply_email = "bogy@google.com"
                    text = "testing only"
                    # user = {"voice_owner_id": user_id, "email": reply_email, "text": text}
                    # user = {"voice_owner_id": user_id, "reply_to_email": reply_email, "message": text}
                    user = {"voice_owner_id": user_id, "reply_email": reply_email, "text": text}
                    resp = Renderer_Functions.feedback(resp["cookies"][0], user)
                    # here we will check if the voice detail is passed or failed
                    resp_status_code = resp["status_code"]
                    print(resp_status_code)

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))


if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)

import unittest
import Renderer_Functions as Renderer_Functions
import requests
from Database import Postgres

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBVersionSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_version(self):

        #No need for token since it is a get() request
        resp = requests.get('http://52.215.133.122/smorphing/2.0/getmydetails').json()

        # Get value from configurationDB
        cursor = Postgres.connect_to_db_configurationdb()
        query = "SELECT val_integer FROM configuration_params WHERE name='VB_VERSION'"
        cursor.execute(query)
        db_info = [r for r in cursor]
        param_db_value = db_info[0][0]

        # here we will check if the request is passed or failed
        if self.assertEqual(resp, param_db_value) is None:
            print("Version Pass")
            print("resp: {0}".format(resp))

    print("Version Pass")


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)